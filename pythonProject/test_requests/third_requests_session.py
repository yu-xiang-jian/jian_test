import requests

# 实例化session对象
session = requests.session()

# 使用session对象发送验证码
response = session.get(url="http://localhost/index.php?m=Home&c=User&a=verify&r=0.29753152039402253")
print(response.cookies)

# 使用session对象发送登录接口请求
response = session.post(url="http://localhost/index.php?m=Home&c=User&a=do_login&t=0.2360693589784304",
                         data={"username": "13800138006", "password": "123456", "verify_code": "888"},
                         headers={"Content-Type": "application/x-www-form-urlencoded"})

print(response.json())