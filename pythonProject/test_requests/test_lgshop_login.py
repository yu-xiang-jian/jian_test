# 导包
import unittest, requests

# 创建继承unittest.TestCase的类
class TestLgshopLogin(unittest.TestCase):

    # unittest框架的测试固件
    def setUp(self):
        # 实例化session对象
        self.session = requests.session()
        # 验证码url
        self.url_verify = "http://localhost/index.php?m=Home&c=User&a=verify&r=0.29753152039402253"
        # 登录url
        self.url_login = "http://localhost/index.php?m=Home&c=User&a=do_login&t=0.2360693589784304"

    # 编写登陆成功的测试用例
    def test001_login_success(self):

        # 使用session对象发送验证码
        response = self.session.get(url=self.url_verify)
        print(response.cookies)

        # 使用session对象发送登录接口请求
        response = self.session.post(url=self.url_login,
                                data={"username": "13800138006", "password": "123456", "verify_code": "8888"},
                                headers={"Content-Type": "application/x-www-form-urlencoded"})

        print(response.json())
        # 断言结果
        self.assertEqual(1, response.json().get("status"))
        self.assertEqual("登陆成功", response.json().get("msg"))
    # 编写手机号未注册的测试用例
    def test002_username_is_not_regist(self):
        # 使用session对象发送验证码
        response = self.session.get(url=self.url_verify)
        print(response.cookies)

        # 使用session对象发送登录接口请求
        response = self.session.post(url=self.url_login,
                                     data={"username": "13290523626", "password": "123456", "verify_code": "8888"},
                                     headers={"Content-Type": "application/x-www-form-urlencoded"})

        print(response.json())
        # 断言结果
        self.assertEqual(-1, response.json().get("status"))
        self.assertEqual("账号不存在!", response.json().get("msg"))
    # 密码错误的测试用例
    def test003_password_is_error(self):
        # 使用session对象发送验证码
        response = self.session.get(url=self.url_verify)
        print(response.cookies)

        # 使用session对象发送登录接口请求
        response = self.session.post(url=self.url_login,
                                     data={"username": "13800138006", "password": "1234567", "verify_code": "8888"},
                                     headers={"Content-Type": "application/x-www-form-urlencoded"})

        print(response.json())
        # 断言结果
        self.assertEqual(-2, response.json().get("status"))
        self.assertEqual("密码错误!", response.json().get("msg"))
    # 验证码错误
    def test004_verify_code_is_error(self):
        # 使用session对象发送验证码
        response = self.session.get(url=self.url_verify)
        print(response.cookies)

        # 使用session对象发送登录接口请求
        response = self.session.post(url=self.url_login,
                                     data={"username": "13800138006", "password": "123456", "verify_code": "888"},
                                     headers={"Content-Type": "application/x-www-form-urlencoded"})

        print(response.json())
        # 断言结果
        self.assertEqual(0, response.json().get("status"))
        self.assertEqual("验证码错误", response.json().get("msg"))
    # 验证码失效/令牌失效
    def test005_verify_is_invalid(self):
        # 使用session对象发送验证码
        response = self.session.get(url=self.url_verify)
        print(response.cookies)

        # 使用session对象发送登录接口请求
        response = requests.post(url=self.url_login,
                                     data={"username": "13800138006", "password": "123456", "verify_code": "8888"},
                                     headers={"Content-Type": "application/x-www-form-urlencoded"},
                                     cookies={"PHPSESSID":"ig55nndb8demlgg2erkke9i054"})

        print(response.json())
        # 断言结果
        self.assertEqual(0, response.json().get("status"))
        self.assertEqual("验证码错误", response.json().get("msg"))