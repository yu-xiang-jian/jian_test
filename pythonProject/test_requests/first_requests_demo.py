import requests

# 发送百度接口
response = requests.get(url="http://www.baidu.com/s", params={"wd": "python"})

# 打印响应数据
print(response.text)

# 打印响应的URL
print(response.url)

# 打印响应的状态码
print(response.status_code)

# 打印cookie
# print(response.cookies)
#
# # 打印响应头
# print(response.headers)

# # 打印字节码
# print(response.content.decode("utf -8"))

# print(response.json())




