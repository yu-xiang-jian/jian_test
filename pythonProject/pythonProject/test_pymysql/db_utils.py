import pymysql

import config
class DButils:
    def __init__(self,
                 host=config.Host,
                 port=config.Port,
                 database=config.DataBase,
                 user=config.user,
                 password=config.password,
                 charset=config.charset):
        self.host = host
        self.port = port
        self.database = database
        self.user = user
        self.password = password
        self.charset = charset
        # 连接代码
        self.conn = pymysql.connect(
            host=self.host,
            port=self.port,
            database=self.database,
            user=self.user,
            password=self.password,
            charset=self.charset
        )

    # search
    def query_data(self, sql):
        cursor=self.conn.cursor()
        # 执行sql
        try:
            cursor.execute(sql)
            print(cursor.fetchone())
        except Exception as e:
            print(e)
        finally:
            cursor.close()
    # insert
    def insert_data(self, sql):
        cursor=self.conn.cursor()
        try:
            cursor.execute(sql)
        except Exception as e:
            print(e)
        finally:
            cursor.close()


    # modify
    def modify_data(self, sql):
        cursor=self.conn.cursor()
        try:
            cursor.execute(sql)
        except Exception as e:
            print(e)
        finally:
            cursor.close()

    # delete
    def delete_data(self, sql):
        cursor=self.conn.cursor()
        try:
            cursor.execute(sql)
        except Exception as e:
            print(e)
        finally:
            cursor.close()
    # conn.close
    def closeDb(self):
        self.conn.close()

if __name__ == '__main__':
    # 实例化
    dbutils = DButils()
    dbutils.query_data("select * from course")
    dbutils.closeDb()