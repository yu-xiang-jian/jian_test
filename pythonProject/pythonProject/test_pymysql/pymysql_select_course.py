# 导包
import pymysql
# 建立连接
conn = pymysql.connect(host="192.168.80.9",
                       port=3307,
                       user="root",
                       password="Lagou@1234",
                       database="ssm_lagou_edu",
                       charset="utf8")
# 获取游标
cursor=conn.cursor()
# 执行sql
cursor.execute("select * from ssm_lagou_edu.course limit 10;")
# 打印结果
"""
打印下一条数据：cursor.fetchone()
打印下面全部数据：cursor.fetchall()
打印数据的记录数：cursor.rowcount
重新指定指针的位置：cursor.rownumber = 0
"""
print("记录数：", cursor.rowcount)
print("打印下一条数据：", cursor.fetchone())
cursor.rownumber = 0
print("打印全部数据：", cursor.fetchall())
# 关闭游标
cursor.close()
# 关闭连接
conn.close()