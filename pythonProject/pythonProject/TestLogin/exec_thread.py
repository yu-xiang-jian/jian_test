import unittest

import threadpool
from threadpool import ThreadPool

from script.Test_login_xlsx import TestLoginXlsx


def get_cases():
    t1 = unittest.TestLoader()
    suite = t1.loadTestsFromTestCase(TestLoginXlsx)
    return suite


def run_case(suite):
    runner = unittest.TextTestRunner()
    runner.run(suite)


if __name__ == '__main__':
    pool = ThreadPool(5)
    suite = get_cases()
    requests = threadpool.makeRequests(run_case, suite)
    for req in requests:
        pool.putRequest(req)
    pool.wait()
