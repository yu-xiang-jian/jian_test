import unittest
# 创建测试套件
import HTMLTestRunner_PY3

from script.Test_login_xlsx import TestLoginXlsx

suite = unittest.TestSuite()
# 用例添加到测试套件
suite.addTests(unittest.makeSuite(TestLoginXlsx))
print(suite)
# 生成报告
with open("./reports/report.html", 'wb') as f:
    runner = HTMLTestRunner_PY3.HTMLTestRunner(f)
    result = runner.run(suite)
    runner.generateReport(suite, result)
