import unittest, requests

from parameterized import parameterized

from Api.edu_login import EduLogin
import utils


class TestLoginXlsx(unittest.TestCase):
    def setUp(self):
        self.ApiLogin = EduLogin()

    @parameterized.expand(utils.read_excel_data("./data/edu_login.xlsx"))
    def test_login(self, phone, password, message):
        # 登录
        # data = f"phone={phone}&password={password}"
        data = {"phone": phone, "password": password}
        response = self.ApiLogin.edu_login(data)
        # print(data)
        print(response.json())
        # 断言
        self.assertEqual(message, response.json().get("message"))
