import xlrd


def read_excel_data(filename):
    book = xlrd.open_workbook(filename)
    sheet1 = book.sheet_by_index(0)
    nrowMax = sheet1.nrows
    result_list = []
    for i in range(1, nrowMax):
        re = sheet1.row_values(i)
        result_list.append(tuple(re))
    return result_list


if __name__ == '__main__':
    print(read_excel_data("./data/edu_login.xlsx"))
