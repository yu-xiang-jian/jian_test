import requests, unittest


class EduLogin(unittest.TestCase):
    def __init__(self):
        self.url_login = "http://192.168.80.11:8080/ssm_web/user/login"

    def edu_login(self, data):
        return requests.post(url=self.url_login, data=data)
