# 读取json数据文件函数
import json

import xlrd


def read_json_data(filename):
    """

    :param filename:
    :return:
    """
    with open(filename, mode='r', encoding='utf-8') as f:  # 是一个IO类型，不是json数据
        jsonData = json.load(f)
        result_list = []
        for data in jsonData:
            result_list.append(tuple(data.values()))

        return result_list


def read_excel_data(filename):
    book = xlrd.open_workbook(filename)
    sheet1 = book.sheet_by_index(0)
    result_list = []
    for i in range(1, sheet1.nrows):
        re = sheet1.row_values(i)
        result_list.append(tuple(re))
    return result_list

#
# if __name__ == '__main__':
#     re = read_json_data("./data/111")
#     print(re)
# if __name__ == '__main__':
#     read_excel_data("./data/login_data.xls")
