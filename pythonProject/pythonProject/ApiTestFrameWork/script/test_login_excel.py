# 导包
import unittest, requests
from api.lgshop_api_login import LgShopLogin
from parameterized import parameterized
import utils


# 创建测试类
class TestLgShopLogin(unittest.TestCase):
    def setUp(self):
        # 实例化封装的登录api
        self.login_api = LgShopLogin()
        # 实例化session
        self.session = requests.Session()

    def tearDown(self):
        self.session.close()

    # 实现登录的测试用例
    @parameterized.expand(utils.read_excel_data("../data/login_data.xlsx"))
    def test001_login(self, case_name, username, password, verify_code, msg, status):
        # 获取验证码
        re = self.login_api.get_verify_session(self.session)
        # 打印验证码cookie
        # print(re.content)
        # print(re.cookies)
        # 登录
        data = f"username={username}&password={password}&verify_code={verify_code}"
        headers = {"Content-type": "application/x-www-form-urlencoded"}
        res = self.login_api.login_session(self.session, data, headers)
        print("{0}：{1}".format(case_name, res.json()))
        # 断言
        self.assertEqual(200, res.status_code)
        self.assertEqual(int(status), res.json().get("status"))
        self.assertEqual(msg, res.json().get("msg"))
