# 列表生成式
lis = [x for x in range(1, 11)]
print(lis)

#  生成器对象--可迭代对象--next方法
lis = (x for x in range(1, 101))
while True:
    try:
        print(next(lis), end=' ')
    except StopIteration:
        break
