# 导包
import unittest, requests
from api.lgshop_api_login import LgShopLogin


# 创建测试类
class TestLgShopLogin(unittest.TestCase):
    def setUp(self):
        # 实例化封装的登录api
        self.login_api = LgShopLogin()
        # 实例化session
        self.session = requests.Session()

    # 实现登录的测试用例
    def test001_login_success(self):
        # 获取验证码
        re = self.login_api.get_verify_session(self.session)
        # 打印验证码cookie
        print(re.content)
        print(re.cookies)
        # 登录
        data = "username=13800138006&password=123456&verify_code=8888"
        headers = {"Content-type": "application/x-www-form-urlencoded"}
        res = self.login_api.login_session(self.session, data, headers)
        print(res.json())
        self.assertEqual(200, res.status_code)
        self.assertEqual(1, res.json().get("status"))
        self.assertEqual("登陆成功", res.json().get("msg"))

    # 密码错误
    def test002_password_is_error(self):
        re = self.login_api.get_verify_session(self.session)
        data = "username=13800138006&password=1234567&verify_code=8888"
        headers = {"Content-type": "application/x-www-form-urlencoded"}
        res = self.login_api.login_session(self.session, data, headers)
        self.assertEqual(200, res.status_code)
        self.assertEqual(-2, res.json().get("status"))
        self.assertEqual("密码错误!", res.json().get("msg"))

    # 验证码错误
    def test003_verify_is_error(self):
        re = self.login_api.get_verify_session(self.session)
        data = "username=13800138006&password=1234567&verify_code=888"
        headers = {"Content-type": "application/x-www-form-urlencoded"}
        res = self.login_api.login_session(self.session, data, headers)
        self.assertEqual(200, res.status_code)
        self.assertEqual(0, res.json().get("status"))
        self.assertIn("验证码错误", res.json().get("msg"))
