# 导包
import unittest, requests
from api.lgshop_api_login import LgShopLogin
from parameterized import parameterized


# 创建测试类
class TestLgShopLogin(unittest.TestCase):
    def setUp(self):
        # 实例化封装的登录api
        self.login_api = LgShopLogin()
        # 实例化session
        self.session = requests.Session()

    def tearDown(self):
        self.session.close()

    # 实现登录的测试用例
    @parameterized.expand([('13800138006', '123456', '8888', 1, '登陆成功'),
                           ('13800138006', '1234567', '8888', -2, '密码错误!')
                           ])
    def test001_login(self, username, password, verify_code, status, msg):
        # 获取验证码
        re = self.login_api.get_verify_session(self.session)
        # 打印验证码cookie
        print(re.content)
        print(re.cookies)
        # 登录
        data = f"username={username}&password={password}&verify_code={verify_code}"
        headers = {"Content-type": "application/x-www-form-urlencoded"}
        res = self.login_api.login_session(self.session, data, headers)
        print(res.json())
        self.assertEqual(200, res.status_code)
        self.assertEqual(status, res.json().get("status"))
        self.assertEqual(msg, res.json().get("msg"))
