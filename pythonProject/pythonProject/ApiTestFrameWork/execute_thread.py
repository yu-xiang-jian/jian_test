# 导包
import threading
import unittest, reports

import HTMLTestRunner_PY3
import threadpool
from threadpool import ThreadPool, makeRequests

import lib.HTMLTestRunner_PY3
from script.test_login_json import TestLgShopLogin
from tomorrow import threads
import threading


def get_case():
    # 创建测试套件
    t1 = unittest.TestLoader()
    suite = t1.loadTestsFromTestCase(TestLgShopLogin)
    # suite = t1.discover("./script/", "test_login_json.py")
    return suite


def run_case(suite):
    with open("./reports/report4.html", 'wb') as f:
        runner = HTMLTestRunner_PY3.HTMLTestRunner(f)
        result = runner.run(suite)
        runner.generateReport(suite, result)
    # t1 = unittest.TextTestRunner()
#     # t1.run(suite)


if __name__ == '__main__':
    # 使用4个线程
    pool = ThreadPool(4)
    suite = get_case()
    print(suite)
    requests = threadpool.makeRequests(run_case, suite)
    for tests in requests:
        print(tests)
        pool.putRequest(tests)
    pool.wait()
