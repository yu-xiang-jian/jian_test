# 封装登录接口固定内容
# 请求方法、url是固定的
# 请求体和请求头动态


# 导包
import requests

session = requests.Session()


# 创建封装的接口类
class LgShopLogin:

    def __init__(self):
        # 定义用到的url，把url固定到封装的接口类当中
        self.url_verify = "http://localhost/index.php?m=Home&c=User&a=verify"
        self.url_login = "http://localhost/index.php?m=Home&c=User&a=do_login"

    # 封装获取验证码的接口
    def get_verify(self):
        response = requests.get(url=self.url_verify)
        return response

    # 封装登录接口
    def login(self, data, headers):
        return requests.post(url=self.url_login, data=data, headers=headers)

    # 使用session封装验证码接口
    def get_verify_session(self, session):
        return session.get(url=self.url_verify)

    # 使用session封装登录接口
    def login_session(self, session, data, headers):
        return session.post(url=self.url_login, data=data, headers=headers)


if __name__ == '__main__':
    lg_api = LgShopLogin()
    response = lg_api.get_verify()
    print(response.content)

    # 获取cookie
    print(response.cookies)

    headers = {"Content-type": "application/x-www-form-urlencoded", "Cookie": "PHPSESSID=m18djrl062k072v4omjspf6ne4;"}
    data = "username=13800138006&password=1234561&verify_code=8888"
    re = lg_api.login(data, headers)
    print(re.json())
