import requests


# 创建封装的接口类
class SessionRegist:
    def __init__(self):
        # 把url固定到封装的接口类中
        self.url_verify = "http://localhost/index.php?m=Home&c=User&a=verify&type=user_reg"
        self.url_regist = "http://localhost/index.php/Home/User/reg.html"

    # 封装获取验证码的接口
    def get_verify_session(self, session):
        return session.get(url=self.url_verify)

    def regist_session(self, session, data, headers):
        return session.post(url=self.url_regist, data=data, headers=headers)


if __name__ == '__main__':
    session = requests.Session()
    sessionregist = SessionRegist()
    re = sessionregist.get_verify_session(session)
    print(re.content)
    data = "auth_code=TPSHOP&scene=1&username=13800001111&verify_code=8888&password=519475228fe35ad067744465c42a19b2&password2=519475228fe35ad067744465c42a19b2"
    headers = {"Content-type": "application/x-www-form-urlencoded"}
    res = sessionregist.regist_session(session, data, headers)
    print(res.json())
