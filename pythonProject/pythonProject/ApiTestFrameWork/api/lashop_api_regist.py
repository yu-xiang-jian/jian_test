import requests


class LgShopRegist:

    def __init__(self):
        self.url_verify = "http://localhost/index.php?m=Home&c=User&a=verify&type=user_reg"
        self.url_regist = "http://localhost/index.php/Home/User/reg.html"

    def get_verify(self):
        return requests.get(url=self.url_verify)

    def regist(self, data, headers):
        return requests.post(url=self.url_regist, data=data, headers=headers)


if __name__ == '__main__':
    lg_regist = LgShopRegist()
    re = lg_regist.get_verify()
    print(re.content)
    print(re.cookies)

    data = "auth_code=TPSHOP&scene=1&username=13800001112&verify_code=8888&password=519475228fe35ad067744465c42a19b2&password2=519475228fe35ad067744465c42a19b2"
    headers = {"Content-type": "application/x-www-form-urlencoded", "Cookie": "PHPSESSID=t6idjt0g6pr0fqn4s53n7t8up2;"}
    res = lg_regist.regist(data, headers)
    print(res.json())
