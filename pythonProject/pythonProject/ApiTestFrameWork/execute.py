# 导包
import threading
import unittest, reports
import lib.HTMLTestRunner_PY3
from script.test_login_json import TestLgShopLogin

# 创建测试套件
suite = unittest.TestSuite()
# 测试用例添加到测试套件
suite.addTests(unittest.makeSuite(TestLgShopLogin))
# 生成报告
with open("./reports/report2.html", 'wb') as f:
    runner = lib.HTMLTestRunner_PY3.HTMLTestRunner(f)
    result = runner.run(suite)
    runner.generateReport(suite, result)
