import threading
import time
import unittest

from threadpool import ThreadPool, makeRequests


def test_task(name):
    print(f"{threading.current_thread().name}:", name)
    time.sleep(1)


class TestTask(unittest.TestCase):
    def test01(self):
        test_task(1)

    def test02(self):
        test_task(2)

    def test03(self):
        test_task(3)

    def test04(self):
        test_task(4)

    def test05(self):
        test_task(5)

    def test06(self):
        test_task(6)

    def test07(self):
        test_task(7)

    def test08(self):
        test_task(8)

    def test09(self):
        test_task(9)

    def test10(self):
        test_task(10)


def loadTests():
    loader = unittest.TestLoader()
    tests = loader.loadTestsFromTestCase(TestTask)
    return tests


def runTests(tests):
    runner = unittest.TextTestRunner()
    result = runner.run(tests)
    return result


def main():
    pool = ThreadPool(10)
    r = loadTests()
    request = makeRequests(runTests, r)
    for req in request:
        pool.putRequest(req)
    pool.wait()


if __name__ == '__main__':
    main()
